num=sample(1:50,10,replace=TRUE)#从1~50内选择10个随机数，且可重复
for(i in 1:9)#借鉴数据结构的冒泡排序算法\\循环语句，遍历的趟数
{
  for(j in (i+1):10)#循环语句，遍历的范围
  {
    if(num[i]<num[j])#条件判断语句，如果是从小到大顺序，执行下列语句，使二者交换位置
    {
      tem=num[i]
      num[i]=num[j]
      num[j]=tem
    }
  }
}
cat("num中10个数从大到小的排序结果为：",num)#输出排序后的结果

