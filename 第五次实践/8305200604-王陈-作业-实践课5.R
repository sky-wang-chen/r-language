setwd("C:\\Users\\Lenovo\\Desktop\\学习\\大三下\\R语言\\实践5")#设置工作路径
x1=read.csv("C:\\Users\\Lenovo\\Desktop\\学习\\大三下\\R语言\\实践3\\dataC.csv",sep=",",header =FALSE)
names(x1)=x1[1,]#设置列名
x1=x1[-1,]
row.names(x1)=x1[,1]#设置行名
x1=x1[,-1]
View(x1)
names(x1)
# if(x1.row.names="ad")
F1=as.matrix(x1)
F2=as.vector(F1)#转为向量
F4=as.numeric(F2)#将向量再次转换成数字型数据
F5=F4[F4!=0]#去掉数据中的0
F6=log(10,F5)#所有数据取对数
View(F6)
library(RColorBrewer)#调用调节颜色的brewer包
hist(F6,col=brewer.pal(10,'Pastel2'),main="不同样品的对应蛋白的表达值")#绘图
#hist(F6,col = "green3",main="不同样品的对应蛋白的表达值")使用另一种颜色绘图

library("openxlsx")
x2=read.xlsx("C:\\Users\\Lenovo\\Desktop\\学习\\大三下\\R语言\\实践4\\GSE67835\\dataw.xlsx")#读取合并后并且整理的数据
row.names(x2)=x2[,1]#设置行名
x2=x2[,-1]
View(x2)
H1=as.matrix(x2)
H2=as.vector(H1)#转为向量
H3=as.numeric(H2)#将向量再次转换成数字型数据
H4=H3[H3!=0]#去掉值为0的数据
H5=log(10,H4)#所有数据取对数
hist(H5,col=brewer.pal(10,'Pastel2'),main="不同基因",sub="基因号")#绘图

#View(K1)#行名有重复，使用以下几行无法修改数据的格式
#row.names(K1)=K1[,1]#设置行名
#K1=K1[,-1]
#K2=as.matrix(K1)
#K3=as.vector(K2)#转为向量
#K4=as.numeric(K3)
load("C:/Users/Lenovo/Desktop/学习/大三下/R语言/实践5/class5_volcano.RData")#加载数据
K=prostat#将读入的R的数据赋给K
library("openxlsx")
View(K)
library(ggplot2)#调用ggplot2包
thres_log2FC=1.2#定义限定值
thres_p=0.05
K$FC=2^K$FC#将FC列的数据还原为原来的数值
K[which(K$FC>=thres_log2FC&K$P<thres_p),"Change"]="up"#设置上调基因
K[which(K$FC<=thres_log2FC&K$FC>=1/1.2&K$P<thres_p),"Change"]="none"
K[which(K$FC<=1/1.2&K$P<thres_p),"Change"]="down"#设置下调基因
K[which(K$P>=thres_p),"Change"]="none"#其余基因设置为空

library("ggrepel")
View(K)
p=ggplot(data=K, aes(x =log2(FC), y = -log10(P), color = Change)) +#设置x轴、y轴数值
  geom_point(size = 1,position = position_jitterdodge (jitter.width =0.1,
                                                          dodge.width =0.3)) +  #数据抖动+闪避，绘制散点图
  scale_color_manual(values = c('red', 'gray', 'green'), limits = c('up', 'none', 'down')) +  #自定义点的颜色
  labs(x = 'log2 Fold Change', y = '-log10(P)', title= 'gleason high vs low',color = 'color') +  #坐标轴标题
  theme(plot.title = element_text(hjust = 0.5, size = 14), panel.grid = element_blank(), #背景色、网格线、图例等主题修改
        panel.background = element_rect(color = 'black', fill = 'transparent'), 
        legend.key = element_rect(fill = 'transparent')) +
  geom_vline(xintercept = c(-log2(thres_log2FC), log2(thres_log2FC)), lty = 4, color = 'black') +  #添加阈值线
  geom_hline(yintercept = -log10(thres_p), lty = 4, color = 'black') +
  geom_text_repel(data= subset(K, K$P<0.05  & (K$FC>=1.2 | K$FC<=1/1.2)),
                  aes(x=log2(FC),y=-log10(P),label = ID),
                  size = 3,
                  box.padding = unit(0.5, "lines"),
                  point.padding = unit(0.8, "lines"), segment.color = "black", show.legend = FALSE)
p
ggsave("火山图.jpg",width=17,height=15)#保存为jpg格式的文件
graphics.off()

p2=pdf('聚类图.pdf')#绘制树状图
dist.r = dist(x1, method="manhattan")#用dist()函数计算变量间距离
hc.r = hclust(dist.r, method ="average")#用hclust()进行聚类
p2=plot(hc.r, hang = -1,labels=NULL)#绘图
p2
dev.off()#关闭文件